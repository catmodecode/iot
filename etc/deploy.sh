APP_CONTAINER=iot_php

# Выполнение команд внутри нового контейнера приложения
if docker exec ${APP_CONTAINER}_new php artisan migrate --force && docker exec ${APP_CONTAINER}_new php artisan optimize
then
    echo "Deployment successful"
else
    echo "Deployment failed, rolling back"
    # Остановить и удалить новый контейнер
    docker stop ${APP_CONTAINER}_new
    docker rm ${APP_CONTAINER}_new
    exit 1
fi
