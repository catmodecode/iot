ARG PIPELINE_ID=none
FROM registry.gitlab.com/catmodecode/iot/common:${PIPELINE_ID}

# nodejs for inertia
RUN apk add nodejs

# Update PHP INI
RUN echo "memory_limit=1024M" >> /usr/local/etc/php/php.ini
RUN echo "max_execution_time=180" >> /usr/local/etc/php/php.ini
RUN echo "upload_max_filesize=100M" >> /usr/local/etc/php/php.ini
RUN echo "post_max_size=200M" >> /usr/local/etc/php/php.ini

RUN echo "zend_extension=opcache" >> /usr/local/etc/php/conf.d/opcache.ini
RUN echo "opcache.enable=1" >> /usr/local/etc/php/conf.d/opcache.ini
RUN echo "opcache.enable_cli=1" >> /usr/local/etc/php/conf.d/opcache.ini
RUN echo "opcache.jit=off" >> /usr/local/etc/php/conf.d/opcache.ini
RUN echo "opcache.jit_buffer_size=128M" >> /usr/local/etc/php/conf.d/opcache.ini
RUN echo "opcache.memory_consumption=512" >> /usr/local/etc/php/conf.d/opcache.ini
RUN echo "opcache.interned_strings_buffer=164" >> /usr/local/etc/php/conf.d/opcache.ini
RUN echo "opcache.max_accelerated_files=65407" >> /usr/local/etc/php/conf.d/opcache.ini

ADD ./etc/php-www.conf /usr/local/etc/php-fpm.d/www.conf

CMD ["php-fpm"]
